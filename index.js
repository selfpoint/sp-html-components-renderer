'use strict';

const HTML_COMPONENTS_TYPES = {
        EDITOR: 1,
        IMAGE: 2,
        PLAIN_HTML: 3
    },
    HTML_COMPONENTS_STYLES = {
        [HTML_COMPONENTS_TYPES.IMAGE]: 'max-width: 100%;',
    };

/**
 * Generate HTML
 * @public
 *
 * @param {Array.<Object>} components
 * @param {number} components.type
 * @param {string} components.value
 * @param {string} components.backgroundColor
 * @param {number} [components.spacing=0]
 *
 * @returns {string}
 */
function generateHtml(components) {
    return components.map(component => {
        const backgroundColor = `background-color: ${component.backgroundColor};`,
            margin = `margin: ${component.spacing || 0}px;`,
            style = `${backgroundColor}${margin}${HTML_COMPONENTS_STYLES[component.type] || ''}`;

        switch (component.type) {
            case HTML_COMPONENTS_TYPES.EDITOR:
            case HTML_COMPONENTS_TYPES.PLAIN_HTML:
                return `<div style="${style}">${component.value}</div>`;

            case HTML_COMPONENTS_TYPES.IMAGE:
                return `<img style="${style}" src="${component.value}"/>`;

            default:
                throw new ReferenceError(`[sp-html-components-generator]: Unknown component type ${component.type}.`);
        }
    }).join('');
}

module.exports.generateHtml = generateHtml;