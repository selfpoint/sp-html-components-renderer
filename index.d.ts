// TODO: generate it from gulp

export declare interface Component {
    type: number;
    value: string;
    backgroundColor: string;
    spacing?: number;
}

export declare function generateHtml(components: Array<Component>): string;
