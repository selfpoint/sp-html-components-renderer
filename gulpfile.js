'use strict';

const gulp = require('gulp'),
    babel = require('gulp-babel'),
    git = require('gulp-git'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    wrap = require('gulp-wrap');

gulp.task('default', ['angular-js', 'angular']);

gulp.task('angular-js', function () {
    return gulp.src('index.js')
        .pipe(babel({presets: ['es2015']}))
        .pipe(wrap("angular.module('spHtmlComponentsRenderer', []).service('spHtmlComponentsRenderer', function () {var module = {exports: this}; <%= contents %>});"))
        .pipe(rename('sp-html-components-renderer.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
});

gulp.task('angular', function () {
    return gulp.src('index.js')
        .pipe(babel({presets: ['es2015']}))
        .pipe(rename('sp-html-components-renderer-export.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
});